# dequoce

Script d'automatisation de la déclaration quotidienne d'état de santé des employés
du Cégep Limoilou pour une réponse négative à TOUTES les questions

Fonctionnel avec le site https://cegeplimoilou.ca/declaration-covid-employes en date du 2021-08-27


# NOTES IMPORTANTES
* À n'utiliser que SI et SEULEMENT SI vous répondez «Non» à toutes les questions du formulaire
* Ce script peut devenir non fonctionnel du jour au lendemain si le site de déclaration change
* Le script ouvrira le site dans Firefox et remplira les formulaires pour vous, mais vous devrez manuellement réviser et confirmer l'envoi à la fin (à moins de mettre le paramètre «soumissionFinaleAuto» à «True»)


# Prérequis pour lancer le script
1. [Firefox](https://www.mozilla.org/en-CA/firefox/new/)
1. [Python 3+](https://www.python.org/downloads/)
1. Selenium pour Python (« pip install selenium »)
1. [WebDriver pour Firefox](https://github.com/mozilla/geckodriver/releases) placé dans le PATH

# Utilisation
1. Modifiez les informations personnelles dans le script afin d'y mettre les vôtres
1. Modifiez les paramètres si désiré
1. Lancez le script comme il vous plaît! (à la demande, tâche automatisée, etc.)
