
# ----- Déquocé v1.0.3 -----

# Script d'automatisation de la déclaration quotidienne d'état de santé des employés
# du Cégep Limoilou pour une réponse négative à TOUTES les questions



# INFORMATIONS PERSONNELLES POUR LE FORMULAIRE
########################################################################

prenomNom = "Prénom Nom"

superieurCampus = "Nom du supérieur, campus de Québec/Charlesbourg"

telephone = "123 456-7890"


# PARAMÈTRES
########################################################################

# Détermine si la soumission finale est faite automatiquement ou manuellemment
#  si «False» (par défaut), alors permet de réviser les réponses avec le bouton «Précédent» avant l'envoi final
soumissionFinaleAuto = False

# Si True, attendra qu'on appuie sur «Entrée» pour quitter le script une fois terminé
# (sinon quitte en même temps qu'on ferme la fenêtre Firefox)
attendreAvantDeFermer = False

# ##############################################################################################




# INITIALISATION
########################################################

import sys
import re
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# Valeurs présumées statiques
urlDeclaration = "https://cegeplimoilou.ca/declaration-covid-employes"
classBoutonContinuer = "Gen_Btn Gen_BtnContinuer"
classBoutonConfirmer = "Gen_Btn Gen_BtnAction pulse"
classPagination = "Gen_PaginationItemChoisi"
idBoiteNom = "TexteChoixRep57276"
idBoiteSuperieurCampus = "TexteChoixRep57277"
idBoiteTelephone = "TexteChoixRep57278"
idBoutonNon = { 2: "ChoixRep57280_2", 3: "ChoixRep57283_2", 4: "ChoixRep57285_2" }

driver = Firefox()

driver.get(urlDeclaration)

# Un objet d'attente avec timeout de 10s
wait = WebDriverWait(driver, 10)

# Attend redirection vers https://climoilou-estd.omnivox.ca/estd/frma/EffectueConsultation.ovx
print("Attend la fin de la redirection... ", end='')
try:
	wait.until(lambda driver: re.search(r'EffectueConsultation\.ovx', driver.current_url))
except:
	erreur("Impossible d'atteindre le formulaire")
print("OK\n")



# CORPS DU PROGRAMME
########################################################################################

def main():

	# PAGE 1
	########################################################

	noPage = 1

	print(f"Page {noPage}... ", end='')

	# Attend de trouver le 1er id pour continuer
	try:
		wait.until(EC.presence_of_element_located((By.ID, idBoiteNom)))
	except:
		erreur(f"(p. {noPage}) Incapable de trouver 1er id")

	# Insère prénom et nom
	try:
		driver.find_element_by_id(idBoiteNom).send_keys(prenomNom);
	except:
		erreur(f"(p. {noPage}) Boite de saisie du nom introuvable")

	# Insère supérieur et campus
	try:
		driver.find_element_by_id(idBoiteSuperieurCampus).send_keys(superieurCampus);
	except:
		erreur(f"(p. {noPage}) Boite de saisie du supérieur et du campus introuvable")

	# Insère no de téléphone
	try:
		driver.find_element_by_id(idBoiteTelephone).send_keys(telephone);
	except:
		erreur(f"(p. {noPage}) Boite de saisie du téléphone introuvable")

	soumettre(noPage)

	print("OK")


	# PAGES 2 à 4 (boutons «Non»)
	#####################################################################

	for noPage in range(2, 5):

		print(f"Page {noPage}... ", end='')

		# Attend d'être sur la bonne page
		try:
			wait.until(EC.presence_of_element_located((By.XPATH, f"//a[(@class='{classPagination}') and (text() = '{noPage}')]")))
		except:
			erreur(f"La page {noPage} attendue n'apparaît pas")

		# Coche le bouton «Non»
		try:
			driver.find_element_by_id(idBoutonNon[noPage]).click()
		except:
			erreur(f"(p. {noPage}) Bouton «Non» introuvable")

		# Soumet la page
		soumettre(noPage)

		print("OK")


	# PAGE 5
	########################################################

	noPage = 5

	print(f"Page {noPage}... ", end='')

	# Attend d'être sur la bonne page
	try:
		wait.until(EC.presence_of_element_located((By.XPATH, f"//a[(@class='{classPagination}') and (text() = '{noPage}')]")))
	except:
		erreur(f"Page {noPage} attendue n'apparaît pas")

	# Soumet la page
	soumettre(noPage)

	print("OK")


	# PAGE 6
	########################################################

	noPage = 6

	print(f"Page {noPage}... ", end='')

	# Attend d'être sur la bonne page
	try:
		wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, f"a[class='{classBoutonConfirmer}']")))
	except:
		erreur(f"Page {noPage} attendue n'apparaît pas")

	# Soumet la page si auto
	if soumissionFinaleAuto:
		soumettre(noPage)

	print("OK")


# ROUTINES
####################################################

# Affiche une erreur et ferme le fureteur
def erreur(msg):
	print("Erreur: " + msg)
	print()
	input("Appuyer sur 'Entrée' pour quitter...")
	print()
	sys.exit()


# Soumet une page
def soumettre(noPage):
	boutonContinuer = driver.find_element_by_css_selector(f"a[class='{classBoutonContinuer}']");
	if boutonContinuer:
		boutonContinuer.click()
	else:
		erreur(f"(p. {noPage}) Bouton Continuer introuvable")

main()

if attendreAvantDeFermer:
  input("\nAppuyer sur 'Entrée' pour terminer...")